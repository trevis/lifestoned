﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-spell-table-search', {
    data() {
        return {
            name: null,
            school: null,
            category: null
        };
    },
    computed: {
    },
    watch: {
    },
    methods: {
        search() {
            this.$spellTable.search(this.name, this.school, this.category);
        }
    },
    template: `
    <div class="well">
        <div class="row row-spacer">
            <div class="col-md-4">Name</div>
            <div class="col-md-2">School</div>
            <div class="col-md-3">Category</div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-4"><input v-model="name" type="text" class="form-control" /></div>
            <div class="col-md-2"><lsd-enum-select v-model="school" type="SpellSchool" keyOnly showEmpty emptyText="All"></lsd-enum-select></div>
            <div class="col-md-3"><lsd-enum-select v-model="category" type="SpellCategory" keyOnly></lsd-enum-select></div>

            <div class="col-md-1 col-md-offset-2"><button @click="search" type="button" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button></div>
            <!-- div class="col-md-2"></div -->
        </div>
    </div>
    `
});

Vue.component('lsd-spell-table', {
    data() {
        return {
            current: this.$spellTable.newSpell()
        };
    },
    computed: {
        results() { return this.$spellTable.table; }
    },
    methods: {
        edit(spell) {
            this.current = spell;
            this.$refs.entry.show();
        }
    },
    template: `
    <div>
    <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
            <tr>
                <th>Id</th>
                <th>Description</th>
                <th>Last Modified</th>
                <th style="width: 16rem; text-wrap:none;">Actions</th>
            </tr>
        </thead>
        <tbody>
        <tr v-for="spell in results">
            <td>{{ spell.key }}</td>
            <td>{{ spell.value.name }}</td>
            <td>{{ spell.lastModified | toDate }}</td>
            <td>
                <div class="btn-group" role="group">
                    <button @click="edit(spell)" type="button" class="btn btn-sm btn-default">
                        <span class="glyphicon glyphicon-edit" title="Edit"></span>
                    </button>

                    <div class="btn-group dropdown" role="group">
                        <button class="btn btn-sm btn-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"><span class="glyphicon glyphicon-download" title="Download"></span>&nbsp;<span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a :href="'/SpellTable/DownloadOriginal/' + spell.key">Original</a></li>
                        </ul>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <lsd-spell-table-entry ref="entry" v-model="current"></lsd-spell-table-entry>
    </div>
    `
});

Vue.component('lsd-spell-table-entry', {
    props: ['entry'],
    model: { prop: 'entry', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
        spell() {
            return this.entry.value;
        },
        title() {
            return `Edit ${this.entry.key} -- ${this.spell.name}`;
        }
    },
    methods: {
        show() {
            this.$refs.modal.show();
        },
        save() {
            this.$spellTable.save(this.entry);
        }
    },
    template: `
    <lsd-dialog ref="modal" :title="title" @saved="save">
        <template v-slot:headerCommands>
        <div class="col-md-offset-2 col-md-4">

            <ul class="nav nav-pills nav-pills-sm" role="group">
                <li class="active"><a data-toggle="tab" href="#general" class="btn btn-primary" title="General Properties"><i class="glyphicon glyphicon-list"></i></a></li>
                <li><a data-toggle="tab" href="#stats" class="btn btn-primary" title="Status Effects"><i class="glyphicon glyphicon-stats"></i></a></li>
                <li><a data-toggle="tab" href="#formula" class="btn btn-primary" title="Formula"><i class="glyphicon glyphicon-book"></i></a></li>
                <li><a data-toggle="tab" href="#meta" class="btn btn-primary" title="Spell Meta"><i class="glyphicon glyphicon-briefcase"></i></a></li>
                <li><a data-toggle="tab" href="#history" class="btn btn-primary" title="Change Log / History"><i class="glyphicon glyphicon-calendar"></i></a></li>
            </ul>
        </div>
        </template>

        <div class="tab-content">

            <lsd-spell-table-general v-model="entry.value" id="general" class="tab-pane fade in active"></lsd-spell-table-general>

            <lsd-spell-table-stats v-model="entry.value" id="stats" class="tab-pane fade"></lsd-spell-table-stats>

            <lsd-spell-table-formula v-model="entry.value" id="formula" class="tab-pane fade"></lsd-spell-table-formula>

            <lsd-spell-table-meta v-model="entry.value.meta_spell" id="meta" class="tab-pane fade"></lsd-spell-table-meta>

            <div id="history" class="tab-pane fade">
                <lsd-changelog v-model="entry"></lsd-changelog>
            </div>

        </div>

    </lsd-dialog>
    `
});

Vue.component('lsd-spell-table-general', {
    props: ['spell'],
    model: { prop: 'spell', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
    },
    template: `
    <div class="row">
        <div class="col-md-6">

        <div class="row">
            <div class="col-md-4">Icon</div>
            <div class="col-md-4">Order</div>
        </div>

        <div class="row">
            <div class="col-md-4"><input v-model="spell.iconID" type="number" class="form-control" /></div>
            <div class="col-md-4"><input v-model="spell.display_order" type="number" class="form-control" /></div>
        </div>

        <div class="row"><div class="col-md-12">Name</div></div>
        <div class="row">
            <div class="col-md-12"><input v-model="spell.name" type="text" class="form-control" /></div>
        </div>

        <div class="row"><div class="col-md-12">Description</div></div>

        <div class="row">
            <div class="col-md-12"><input v-model="spell.desc" type="text" class="form-control" /></div>
        </div>

        </div>

        <div class="col-md-3">
            <div class="row">School</div>
            <div class="row"><lsd-enum-select v-model="spell.school" type="SpellSchool" keyOnly></lsd-enum-select></div>

            <div class="row">Category</div>
            <div class="row"><lsd-enum-select v-model="spell.category" type="SpellCategory" keyOnly></lsd-enum-select></div>

            <div class="row">Flags</div>
            <div class="row"><lsd-flags-select v-model="spell.bitfield" type="SpellFlags"></lsd-flags-select></div>

            <div class="row">Target Type</div>
            <div class="row">
                <lsd-flags-select v-model="spell.non_component_target_type" type="ItemType"></lsd-flags-select>
            </div>
        </div>

    </div>
    `
});

Vue.component('lsd-spell-table-stats', {
    props: ['spell'],
    model: { prop: 'spell', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
    },
    template: `
    <div>
        <div class="row">
            <div class="col-md-3">Power</div>
            <div class="col-md-3">Economy</div>
            <div class="col-md-3">Burn Rate</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="spell.power" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="spell.spell_economy_mod" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="spell.component_loss" type="number" step="0.01" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Caster Effect</div>
            <div class="col-md-3">Fizzle Effect</div>
            <div class="col-md-3">Target Effect</div>
        </div>

        <div class="row">
            <div class="col-md-3"><lsd-enum-select v-model="spell.caster_effect" type="PhysicsScriptType" keyOnly></lsd-enum-select></div>
            <div class="col-md-3"><input v-model="spell.fizzle_effect" type="number" class="form-control" /></div>
            <div class="col-md-3"><lsd-enum-select v-model="spell.target_effect" type="PhysicsScriptType" keyOnly></lsd-enum-select></div>
        </div>

        <div class="row">
            <div class="col-md-3">Mana</div>
            <div class="col-md-3">Mana Mod</div>
            <div class="col-md-3">Range</div>
            <div class="col-md-3">Range Mod</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="spell.base_mana" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="spell.mana_mod" type="number" step="0.001" class="form-control" /></div>
            <div class="col-md-3"><input v-model="spell.base_range_constant" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="spell.base_range_mod" type="number" step="0.01" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Recovery Amount</div>
            <div class="col-md-3">Recovery Time</div>
            
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="spell.recovery_amount" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="spell.recovery_interval" type="number" class="form-control" /></div>
            
        </div>

    </div>
    `
});

Vue.component('lsd-spell-table-meta', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
        smod() {
            return this.data.spell.smod || { key: null, type: null, val: null };
        }
    },
    methods: {
    },
    template: `
    <div>

        <div class="row">
            <div class="col-md-4">Type</div>
        </div>

        <div class="row">
            <div class="col-md-4"><lsd-enum-select v-model="data.sp_type" type="SpellType" keyOnly></lsd-enum-select></div>
        </div>

        <lsd-spell-table-meta-enchant v-if="data.sp_type == 1 || data.sp_type == 12" v-model="data.spell"></lsd-spell-table-meta-enchant>
        <lsd-spell-table-meta-projectile v-else-if="data.sp_type == 2" v-model="data.spell"></lsd-spell-table-meta-projectile>
        <lsd-spell-table-meta-boost v-else-if="data.sp_type == 3 || data.sp_type == 11" v-model="data.spell"></lsd-spell-table-meta-boost>
        <lsd-spell-table-meta-transfer v-else-if="data.sp_type == 4" v-model="data.spell"></lsd-spell-table-meta-transfer>
        <lsd-spell-table-meta-portal-link v-else-if="data.sp_type == 5 || data.sp_type == 6" v-model="data.spell"></lsd-spell-table-meta-portal-link>
        <lsd-spell-table-meta-portal-summon v-else-if="data.sp_type == 7" v-model="data.spell"></lsd-spell-table-meta-portal-summon>
        <lsd-spell-table-meta-portal-send v-else-if="data.sp_type == 8 || data.sp_type == 13" v-model="data.spell"></lsd-spell-table-meta-portal-send>
        <lsd-spell-table-meta-dispell v-else-if="data.sp_type == 9 || data.sp_type == 14" v-model="data.spell"></lsd-spell-table-meta-dispell>
        <lsd-spell-table-meta-projectile-life v-else-if="data.sp_type == 10" v-model="data.spell"></lsd-spell-table-meta-projectile-life>
        <lsd-spell-table-meta-projectile-enchant v-else-if="data.sp_type == 15" v-model="data.spell"></lsd-spell-table-meta-projectile-enchant>

    </div>
    `
});

Vue.component('lsd-spell-table-formula', {
    props: ['spell'],
    model: { prop: 'spell', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
    },
    template: `
    <div class="row">

        <div class="col-md-3">
            <div class="row row-spacer">Version</div>
            <div class="row row-spacer"><input v-model="spell.formula_version" type="number" class="form-control" /></div>

            <div class="row row-spacer">Components</div>
            <div v-for="(comp, idx) in spell.formula" class="row row-spacer">
                <lsd-enum-select v-model="spell.formula[idx]" type="SpellComponent" keyOnly></lsd-enum-select>
            </div>

        </div>

    </div>
    `
});

Vue.component('lsd-spell-table-meta-enchant', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
        smod() {
            return this.data.smod || { key: null, type: null, val: null };
        }
    },
    methods: {
    },
    template: `
    <div>

        <div class="row">
            <div class="col-md-3">Duration</div>
            <div class="col-md-3">Degrade Limit</div>
            <div class="col-md-3">Degrade Mod</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.duration" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.degrade_limit" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.degrade_modifier" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Stat</div>
            <div class="col-md-3">Type</div>
            <div class="col-md-3">Value</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="smod.key" type="number" class="form-control" /></div>
            <div class="col-md-3"><lsd-flags-select v-model="smod.type" type="EnchantmentType"></lsd-flags-select></div>
            <div class="col-md-3"><input v-model="smod.val" type="number" step="any" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Damage Type</div>
        </div>

        <div class="row">
            <div class="col-md-3"><lsd-flags-select v-model="data.dtype" type="DamageType"></lsd-flags-select></div>
        </div>

    </div>
    `
});

Vue.component('lsd-spell-table-meta-projectile', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
    },
    template: `
    <div>

        <div class="row">
            <div class="col-md-3">Element</div>
            <div class="col-md-3">Damage</div>
            <div class="col-md-3">Variance</div>
            <div class="col-md-3">Weenie Id</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.etype" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.baseIntensity" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.variance" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.wcid" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Imbued Effect</div>
            <div class="col-md-3">Elemental Mod</div>
            <div class="col-md-3">Ignore Resist</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.imbuedEffect" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.elementalModifier" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.ignoreMagicResist" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Slayer Type</div>
            <div class="col-md-3">Slayer Mod</div>
            <div class="col-md-3">Crit Freq</div>
            <div class="col-md-3">Crit Multiplier</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.slayerCreatureType" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.slayerDamageBonus" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.critFreq" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.critMultiplier" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Count</div>
            <div class="col-md-3">Variance</div>
            <div class="col-md-3">Tracking</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.numProjectiles" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.numProjectilesVariance" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.bNonTracking" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Spread</div>
            <div class="col-md-3">Vertical</div>
            <div class="col-md-3">Launch</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.spreadAngle" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.verticalAngle" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.defaultLaunchAngle" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-4">Offset</div>
            <div class="col-md-4">Padding</div>
        </div>

        <div class="row">
            <div class="col-md-4"><lsd-vector v-model="data.createOffset"></lsd-vector></div>
            <div class="col-md-4"><lsd-vector v-model="data.padding"></lsd-vector></div>
        </div>

        <div class="row">
            <div class="col-md-4">Dimensions</div>
            <div class="col-md-4">Peturbation</div>
        </div>

        <div class="row">
            <div class="col-md-4"><lsd-vector v-model="data.dims"></lsd-vector></div>
            <div class="col-md-4"><lsd-vector v-model="data.peturbation"></lsd-vector></div>
        </div>

    </div>
    `
});

Vue.component('lsd-spell-table-meta-boost', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
    },
    template: `
    <div>

        <div class="row">
            <div class="col-md-3">Stat</div>
            <div class="col-md-3">Base</div>
            <div class="col-md-3">Variance</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.dt" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.boost" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.boostVariance" type="number" class="form-control" /></div>
        </div>

    </div>
    `
});

Vue.component('lsd-spell-table-meta-transfer', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
    },
    template: `
    <div>

        <div class="row">
            <div class="col-md-3">Source</div>
            <div class="col-md-3">Target</div>
            <div class="col-md-3">Takes</div>
            <div class="col-md-3">Lost</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.src" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.dest" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.proportion" type="number" step="0.0001" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.lossPercent" type="number" step="0.0001" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Source Loss</div>
            <div class="col-md-3">Transfer Cap</div>
            <div class="col-md-3">Max Boost</div>
            <div class="col-md-3">Flags</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.sourceLoss" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.transferCap" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.maxBoostAllowed" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.bitfield" type="number" class="form-control" /></div>
        </div>

    </div>
    `
});

Vue.component('lsd-spell-table-meta-portal-link', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
    },
    template: `
    <div>

        <div class="row">
            <div class="col-md-3">Which</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.index" type="number" class="form-control" /></div>
        </div>

    </div>
    `
});

Vue.component('lsd-spell-table-meta-portal-summon', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
    },
    template: `
    <div>

        <div class="row">
            <div class="col-md-3">Which</div>
            <div class="col-md-3">Lifetime</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.link" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.portal_lifetime" type="number" class="form-control" /></div>
        </div>

    </div>
    `
});

Vue.component('lsd-spell-table-meta-portal-send', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
    },
    template: `
    <div>

        <div class="row">
            <div class="col-md-10">Position</div>
        </div>

        <div class="row">
            <div class="col-md-10"><lsd-position v-model="data.pos"></lsd-position></div>
        </div>

    </div>
    `
});

Vue.component('lsd-spell-table-meta-dispell', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
    },
    template: `
    <div>

        <div class="row">
            <div class="col-md-3">School</div>
            <div class="col-md-3">Power Min</div>
            <div class="col-md-3">Max</div>
            <div class="col-md-3">Variance</div>
        </div>

        <div class="row">
            <div class="col-md-3"><lsd-enum-select v-model="data.school" type="SpellSchool" keyOnly showEmpty></lsd-enum-select></div>
            <div class="col-md-3"><input v-model="data.min_power" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.max_power" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.power_variance" type="number" step="0.0001" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Number</div>
            <div class="col-md-3">Variance</div>
            <div class="col-md-3">Align</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.number" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.number_variance" type="number" step="0.0001" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.align" type="number" class="form-control" /></div>
        </div>

    </div>
    `
});

Vue.component('lsd-spell-table-meta-projectile-life', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
    },
    template: `
    <div>

        <div class="row">
            <div class="col-md-3">Takes</div>
            <div class="col-md-3">Applies</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.drain_percentage" type="number" step="0.0001" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.damage_ratio" type="number" step="0.0001" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Element</div>
            <div class="col-md-3">Damage</div>
            <div class="col-md-3">Variance</div>
            <div class="col-md-3">Weenie Id</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.etype" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.baseIntensity" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.variance" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.wcid" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Imbued Effect</div>
            <div class="col-md-3">Elemental Mod</div>
            <div class="col-md-3">Ignore Resist</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.imbuedEffect" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.elementalModifier" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.ignoreMagicResist" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Slayer Type</div>
            <div class="col-md-3">Slayer Mod</div>
            <div class="col-md-3">Crit Freq</div>
            <div class="col-md-3">Crit Multiplier</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.slayerCreatureType" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.slayerDamageBonus" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.critFreq" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.critMultiplier" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Count</div>
            <div class="col-md-3">Variance</div>
            <div class="col-md-3">Tracking</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.numProjectiles" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.numProjectilesVariance" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.bNonTracking" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Spread</div>
            <div class="col-md-3">Vertical</div>
            <div class="col-md-3">Launch</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.spreadAngle" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.verticalAngle" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.defaultLaunchAngle" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-4">Offset</div>
            <div class="col-md-4">Padding</div>
        </div>

        <div class="row">
            <div class="col-md-4"><lsd-vector v-model="data.createOffset"></lsd-vector></div>
            <div class="col-md-4"><lsd-vector v-model="data.padding"></lsd-vector></div>
        </div>

        <div class="row">
            <div class="col-md-4">Dimensions</div>
            <div class="col-md-4">Peturbation</div>
        </div>

        <div class="row">
            <div class="col-md-4"><lsd-vector v-model="data.dims"></lsd-vector></div>
            <div class="col-md-4"><lsd-vector v-model="data.peturbation"></lsd-vector></div>
        </div>

    </div>
    `
});


Vue.component('lsd-spell-table-meta-projectile-enchant', {
    props: ['data'],
    model: { prop: 'data', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
        smod() {
            return this.data.smod || { key: null, type: null, val: null };
        }
    },
    methods: {
    },
    template: `
    <div>

        <div class="row">
            <div class="col-md-3">Duration</div>
            <div class="col-md-3">Degrade Limit</div>
            <div class="col-md-3">Degrade Mod</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.duration" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.degrade_limit" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.degrade_modifier" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Stat</div>
            <div class="col-md-3">Type</div>
            <div class="col-md-3">Value</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="smod.key" type="number" class="form-control" /></div>
            <div class="col-md-3"><lsd-flags-select v-model="smod.type" type="EnchantmentType"></lsd-flags-select></div>
            <div class="col-md-3"><input v-model="smod.val" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Element</div>
            <div class="col-md-3">Damage</div>
            <div class="col-md-3">Variance</div>
            <div class="col-md-3">Weenie Id</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.etype" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.baseIntensity" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.variance" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.wcid" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Imbued Effect</div>
            <div class="col-md-3">Elemental Mod</div>
            <div class="col-md-3">Ignore Resist</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.imbuedEffect" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.elementalModifier" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.ignoreMagicResist" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Slayer Type</div>
            <div class="col-md-3">Slayer Mod</div>
            <div class="col-md-3">Crit Freq</div>
            <div class="col-md-3">Crit Multiplier</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.slayerCreatureType" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.slayerDamageBonus" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.critFreq" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.critMultiplier" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Count</div>
            <div class="col-md-3">Variance</div>
            <div class="col-md-3">Tracking</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.numProjectiles" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.numProjectilesVariance" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.bNonTracking" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-3">Spread</div>
            <div class="col-md-3">Vertical</div>
            <div class="col-md-3">Launch</div>
        </div>

        <div class="row">
            <div class="col-md-3"><input v-model="data.spreadAngle" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.verticalAngle" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="data.defaultLaunchAngle" type="number" class="form-control" /></div>
        </div>

        <div class="row">
            <div class="col-md-4">Offset</div>
            <div class="col-md-4">Padding</div>
        </div>

        <div class="row">
            <div class="col-md-4"><lsd-vector v-model="data.createOffset"></lsd-vector></div>
            <div class="col-md-4"><lsd-vector v-model="data.padding"></lsd-vector></div>
        </div>

        <div class="row">
            <div class="col-md-4">Dimensions</div>
            <div class="col-md-4">Peturbation</div>
        </div>

        <div class="row">
            <div class="col-md-4"><lsd-vector v-model="data.dims"></lsd-vector></div>
            <div class="col-md-4"><lsd-vector v-model="data.peturbation"></lsd-vector></div>
        </div>

    </div>
    `
});

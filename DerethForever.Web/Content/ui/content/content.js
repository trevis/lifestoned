﻿/// <reference path="../../../Scripts/jquery-3.2.1.js" />
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-content-filter', {
    props: ['selected', 'patches'],
    model: { prop: 'selected', event: 'changed' },
    data() {
        return {
            current: null,
            content: {}
        };
    },
    methods: {
        selectionChanged(event) {
            var o = $(event.target.selectedOptions[0]);
            this.$emit('changed', o.val());            
        }
    },
    created() {

    },
    mounted() {
    },
    template: `
    <select :value="selected" @change="selectionChanged">
        <option value=""></option>
        <option v-for="patch in patches" :value="patch.Id">{{ patch.Name }}</option>
    </select>
    `
});

Vue.component('lsd-content-sheet', {
    props: ['results'],
    data() {
        return {
            current: null,
            content: {}
        };
    },
    methods: {
        selected(patch) {
            if (patch === this.current) return;

            // TODO: Force save?
            if (this.current) this.content[this.current.Id] = null;
            this.current = patch;

            let $this = this;
            this.$content.fetch(patch.Id).then((res) => {
                $this.$set($this.content, patch.Id, res);
            });
        }
    },
    created() {

    },
    mounted() {
    },
    template: `
    <div class="panel-group">
        <div v-for="item in results" class="panel panel-default">
            <div class="panel-heading" @click="selected(item)">{{ item.Name }} - {{ item.ReleaseDate | toDate }}</div>
            <lsd-content-items v-if="current && current.Id===item.Id" v-model="content[item.Id]" :patchId="item.Id"></lsd-content-items>
        </div>
    </div>
    `
});

Vue.component('lsd-content-items', {
    props: ['items', 'patchId'],
    model: { prop: 'items', event: 'changed' },
    data() {
        return {
            contentType: null,
            contentId: null,
            name: null
        };
    },
    methods: {
        isAssigned(item) {
            return item.UserAssigned && item.UserAssigned !== '00000000-0000-0000-0000-000000000000';
        },
        add() {
            let $this = this;
            let item = {
                PatchId: this.patchId,
                ContentType: this.contentType,
                ContentId: this.contentId,
                LastModified: `/Date(${new Date().getTime()})/`,
                UserModified: null,
                Name: this.name,
                UserAssigned: null,
                Complete: false,
                CompleteDate: null
            };
            this.$content.saveContent(item).then(() => {
                if (!$this.items) $this.items = [];
                $this.items.push(item);

                $this.contentId = null;
                $this.name = null;
            }).catch((status) => {
                alert(`Unable to add content. Are you logged in?`);
            }).finally(() => {
                $this.$refs.contentId.focus();
            });
        },
        checkOut(item) {
            this.$content.checkOutContent(item).then(() => {
                item.UserAssigned = null;
            }).catch((status) => {
                alert(`Unable to check-out content. Are you logged in?`);
            });
        },
        checkIn(item) {
            this.$content.checkInContent(item).then((result) => {
                
            }).catch((status) => {
                alert(`Unable to check-in content. Are you logged in / the active user?`);
            });
        },
        deleted(item, idx) {
            this.$content.deleteContent(item).then(() => {
                this.items.splice(idx, 1);
            }).catch((status) => {
                alert(`Unable to delete content. Are you logged in?`);
            });
        }
    },
    created() {

    },
    mounted() {
    },
    template: `
    <div>
    <form @submit.prevent="add">
        <table class="table table-bordered table-striped table-hover table-condensed">
        <thead>
            <tr>
                <th style="width: 12rem">Type</th>
                <th style="width: 12rem">ID</th>
                <th>Name</th>
                <th style="width: 10rem">Assigned</th>
                <th style="width: 8rem">Complete</th>
                <th style="width: 6rem"></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td><lsd-enum-select v-model="contentType" type="ContentType" keyOnly></lsd-enum-select></td>
                <td><input ref="contentId" v-model="contentId" type="number" class="form-control" /></td>
                <td><input v-model="name" type="text" class="form-control" /></td>
                <td><button type="submit" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-plus"></i></button></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </tfoot>
        <tbody>
            <tr v-for="(item, idx) in items">
                <td v-lsd-enum-display="{ type: 'ContentType', key: item.ContentType }">{{ item.ContentType }}</td>
                <td>{{ item.ContentId >>> 0 }}</td>
                <td>{{ item.Name }}</td>
                <td>{{ isAssigned(item) | yn }}</td>
                <td>{{ item.Complete | yn }}</td>
                <td>
                    <div class="btn-group" role="group">
                        <button v-if="isAssigned(item)" @click.prevent="checkIn(item)" type="button" class="btn btn-xs btn-secondary"><span class="glyphicon glyphicon-log-in" title="Check In"></span></button>
                        <button v-else @click.prevent="checkOut(item)" type="button" class="btn btn-xs btn-secondary"><span class="glyphicon glyphicon-log-out" title="Check Out"></span></button>
                        <button @click.prevent="deleted(item, idx)" type="button" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash" title="Delete"></span></button>
                    </div>
                </td>
            </tr>
        </tbody>
        </table>
    </form>
    </div>
    `
});

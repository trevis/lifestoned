﻿
/// <reference path="../../../Scripts/vue.js" />

const VITAL_CALC = [
    (a) => a.Endurance.Ranks / 2,
    (a) => a.Endurance.Ranks,
    (a) => a.Self.Ranks
];

const SKILL_CALC = {
    6: (a) => (a.Coordination.Ranks + a.Quickness.Ranks) / 3,
    7: (a) => (a.Coordination.Ranks + a.Quickness.Ranks) / 5,
    14: (a) => (a.Focus.Ranks) / 3,
    15: (a) => (a.Focus.Ranks + a.Self.Ranks) / 7,
    16: (a) => (a.Focus.Ranks + a.Self.Ranks) / 6,
    21: (a) => (a.Coordination.Ranks + a.Focus.Ranks) / 3,
    22: (a) => (a.Coordination.Ranks + a.Strength.Ranks) / 2,
    23: (a) => (a.Coordination.Ranks + a.Focus.Ranks) / 3,
    24: (a) => (a.Quickness.Ranks),
    31: (a) => (a.Focus.Ranks + a.Self.Ranks) / 4,
    32: (a) => (a.Focus.Ranks + a.Self.Ranks) / 4,
    33: (a) => (a.Focus.Ranks + a.Self.Ranks) / 4,
    34: (a) => (a.Focus.Ranks + a.Self.Ranks) / 4,
    37: (a) => (a.Focus.Ranks + a.Coordination.Ranks) / 3,
    38: (a) => (a.Focus.Ranks + a.Coordination.Ranks) / 3,
    39: (a) => (a.Focus.Ranks + a.Coordination.Ranks) / 3,
    41: (a) => (a.Strength.Ranks + a.Coordination.Ranks) / 3,
    43: (a) => (a.Focus.Ranks + a.Self.Ranks) / 4,
    44: (a) => (a.Strength.Ranks + a.Coordination.Ranks) / 3,
    45: (a) => (a.Strength.Ranks + a.Coordination.Ranks) / 3,
    46: (a) => (a.Coordination.Ranks + a.Quickness.Ranks) / 3,
    47: (a) => (a.Coordination.Ranks) / 2,
    48: (a) => (a.Strength.Ranks + a.Coordination.Ranks) / 2,
    49: (a) => (a.Coordination.Ranks + a.Coordination.Ranks) / 3,
    50: (a) => (a.Strength.Ranks + a.Quickness.Ranks) / 3,
    51: (a) => (a.Coordination.Ranks + a.Quickness.Ranks) / 3,
    52: (a) => (a.Strength.Ranks + a.Coordination.Ranks) / 3,
    54: (a) => (a.Endurance.Ranks + a.Self.Ranks) / 3,
};

Vue.component('lsd-attr', {
    props: ['attribute', 'name'],
    model: { prop: 'attribute', event: 'changed' },
    template: `
    <div>
        <div class="col-md-2">{{ name }}</div>
        <div class="col-md-2">
            <input v-model.number="attribute.Ranks" type="text" class="form-control" />
        </div>
        <slot :attribute="attribute"></slot>
    </div>
    `
});

Vue.component('lsd-vital', {
    props: {
        'vital': { type: Object },
        'name': { type: String },
        'calcBase': { type: Function, default: () => 0 }
    },
    model: { prop: 'vital', event: 'changed' },
    template: `
    <lsd-attr v-model="vital" :name="name">
    <template slot-scope="p">
        <div class="col-md-2">
            <input v-model.number="p.attribute.Current" type="text" class="form-control" />
        </div>
        <div class="col-md-2">
            <input :value="p.attribute.Ranks + calcBase()" type="text" class="form-control" readonly />
        </div>
    </template>
    </lsd-attr>
    `
});

Vue.component('lsd-attributes', {
    props: ['attributes'],
    model: { prop: 'attributes', event: 'changed' },
    methods: {
        calc(idx) {
            return Math.floor(VITAL_CALC[idx](this.attributes));
        }
    },
    template: `
    <div>
        <div class="row row-spacer">
            <div class="col-md-2"><strong>Attribute</strong></div>
            <div class="col-md-2"><strong>Base</strong></div>
            <div class="col-md-2"><strong>Vital</strong></div>
            <div class="col-md-2"><strong>Base</strong></div>
            <div class="col-md-2"><strong>Current</strong></div>
            <div class="col-md-2"><strong>Max</strong></div>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Strength" :attribute="attributes.Strength"></lsd-attr>
        <lsd-vital name="Health" v-model="attributes.Health" :calc-base="() => calc(0)"></lsd-vital>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Endurance" :attribute="attributes.Endurance"></lsd-attr>
        <lsd-vital name="Stamina" v-model="attributes.Stamina" :calc-base="() => calc(1)"></lsd-vital>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Coordination" :attribute="attributes.Coordination"></lsd-attr>
        <lsd-vital name="Mana" v-model="attributes.Mana" :calc-base="() => calc(2)"></lsd-vital>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Quickness" :attribute="attributes.Quickness"></lsd-attr>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Focus" :attribute="attributes.Focus"></lsd-attr>
        </div>
        <div class="row row-spacer">
        <lsd-attr name="Self" :attribute="attributes.Self"></lsd-attr>
        </div>
    </div>
    `
});

Vue.component('lsd-skill', {
    props: {
        'skill': { type: Object },
        'calcBase': { type: Function, default: () => 0 }
    },
    model: { prop: 'skill', event: 'changed' },
    template: `
    <div class="row row-spacer">
        <div v-lsd-enum-display="{ type: 'SkillId', key: skill.SkillId }" class="col-md-2">{{ skill.SkillName }}</div>
        <div class="col-md-2">
            <lsd-enum-select type="SkillStatus" v-model="skill.Skill.TrainedLevel" keyOnly></lsd-enum-select>
        </div>
        <div class="col-md-2">
            <input v-model.number="skill.Skill.Ranks" type="text" class="form-control" />
        </div>
        <div class="col-md-2">
            <input :value="skill.Skill.Ranks + calcBase()" type="text" class="form-control" readonly />
        </div>
        <lsd-prop-delete @click="$emit('removed')"></lsd-prop-delete>
    </div>
    `
});

Vue.component('lsd-skills', {
    props: ['skills', 'attributes'],
    model: { prop: 'skills', event: 'changed' },
    methods: {
        addNew(event) {
            var skills = this.skills || [];
            skills.push(this.$weenie.newSkill(event));
            if (!this.skills) this.$emit('changed', skills);
        },
        calc(id) {
            var fn = SKILL_CALC[id] || ((a) => 0);
            return Math.floor(fn(this.attributes));
        },
        removed(idx) {
            this.skills.splice(idx, 1);
        }
    },
    template: `
    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Skills</h3></div>
        <div class="panel-body">
            <div class="row row-spacer">
                <div class="col-md-2"><strong>Name</strong></div>
                <div class="col-md-2"><strong>Spec</strong></div>
                <div class="col-md-2"><strong>Base</strong></div>
                <div class="col-md-2"><strong>Effective</strong></div>
            </div>
            <lsd-skill v-for="(skill, idx) in skills" :key="skill.SkillId" :idx="idx" v-model="skills[idx]" :calc-base="() => calc(skill.SkillId)" @removed="removed(idx)"></lsd-skill>
        </div>
        <lsd-prop-add :vals="skills" name="Skill" type="SkillId" @added="addNew"></lsd-prop-add>
    </div>
    `
});

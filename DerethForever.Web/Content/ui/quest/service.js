﻿
/// <reference path="../../../Scripts/vue.js" />

function makeQuestTableService() {
    return new Vue({
        data() {
            return {
                table: []
            };
        },
        watch: {
        },
        methods: {
            search(name) {
                var $this = this;
                $.getJSON("/Quest/Search", { name },
                    function (res) {
                        $this.table = res;
                    });
            },
            fetch(id) {
                return new Promise((resolve, reject) => {
                    $.getJSON("/Quest/Get", { id: id },
                        function (res) {
                            resolve(res);
                        })
                        .fail(() => reject());
                });
            },
            save(entry) {
                $.ajax({
                    type: "PUT",
                    url: "/Quest/Put",
                    data: JSON.stringify(entry),
                    contentType: 'application/json',
                    processData: false,
                    success: function (data, status, xhr) {
                        alert('Save Successful');
                        console.log(data, status);
                    },
                    error: function (xhr, status, err) {
                        alert('Save Failed');
                        console.error(status, err);
                    }
                });
            },
            newQuest() {
                return {
                    id: null,
                    key: null,
                    lastModified: null,
                    modifiedBy: null,
                    userChangeSummary: null,
                    isDone: null,
                    changelog: [],
                    value: {
                        fullname: null,
                        maxsolves: null,
                        mindelta: null
                    }
                };
            }
        },
        created() {
        }
    });
}

const QuestTableStore = {
    install(Vue, options) {
        Vue.mixin({
            beforeCreate() {
                const opts = this.$options;
                if (opts.questTableStore) {
                    this.$questTable = opts.questTableStore;
                } else if (opts.parent && opts.parent.$questTable) {
                    this.$questTable = opts.parent.$questTable;
                }
            }
        });
    }
};

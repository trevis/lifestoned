/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Configuration;
using System.IdentityModel.Services;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

using log4net;

using Lifestoned.DataModel.Gdle;
using Lifestoned.DataModel.Gdle.Quests;
using Lifestoned.DataModel.Gdle.Recipes;
using Lifestoned.DataModel.Gdle.Spawns;
using Lifestoned.DataModel.Gdle.Spells;
using Lifestoned.DataModel.Shared;
using Lifestoned.Lib;
using Lifestoned.Providers;
using Lifestoned.Providers.Database;

namespace DerethForever.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private ILog log = LogManager.GetLogger("Application");

        protected void Application_Start()
        {
            log.Info("Starting");

            DapperMappingConfig.RegisterMapping();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            string cacheDir = ConfigurationManager.AppSettings["SandboxCacheDir"];
            string apiEndpoint = ConfigurationManager.AppSettings["DerethForever.Api"];
            string finalDir = ConfigurationManager.AppSettings["FinalDir"];

            IContentProvider provider = new LocalContentProvider(finalDir);

            SandboxContentProviderHost.CurrentProvider = new SandboxContentProvider(provider, cacheDir);

            ContentProviderHost.CurrentProvider = SandboxContentProviderHost.CurrentProvider;
            ////ContentProviderHost.ManagedWorldProvider = new ManagedWorldProvider();

            // this one does not follow the pattern
            ContentProviderHost.PatchContentProvider = new DatabasePatchContentProvider();

            // TODO: Load providers based on web.config settings
            ContentProviderHost.AddProvider(WeenieChange.TypeName, new DatabaseWeenieProvider());
            ContentProviderHost.AddProvider(SpawnMapChange.TypeName, new DatabaseSpawnMapProvider());
            ContentProviderHost.AddProvider(RecipeChange.TypeName, new DatabaseRecipeProvider());
            ContentProviderHost.AddProvider(SpellTableChange.TypeName, new DatabaseSpellTableProvider());
            ContentProviderHost.AddProvider(QuestChange.TypeName, new DatabaseQuestTableProvider());

            SandboxContentProviderHost.AddProvider(WeenieChange.TypeName, new DatabaseWeenieSandboxProvider());
            SandboxContentProviderHost.AddProvider(SpawnMapChange.TypeName, new DatabaseSpawnMapSandboxProvider());
            SandboxContentProviderHost.AddProvider(RecipeChange.TypeName, new DatabaseRecipeSandboxProvider());
            SandboxContentProviderHost.AddProvider(SpellTableChange.TypeName, new DatabaseSpellTableSandboxProvider());
            SandboxContentProviderHost.AddProvider(QuestChange.TypeName, new DatabaseQuestTableSandboxProvider());

            // AuthProviderHost.PrimaryAuthProvider = new DummyAuthProvider();

            PortalDatReader.Initialize(ConfigurationManager.AppSettings["PortalDat"]);

            // To handle exceptions
            GlobalFilters.Filters.Add(new CustomErrorHandler.CustomExceptionFilter());

            System.Threading.ThreadPool.QueueUserWorkItem((state) =>
            {
                // give things time to start
                System.Threading.Thread.Sleep(30000);

                IContentProvider op = provider;
                ISandboxContentProvider osp = SandboxContentProviderHost.CurrentProvider;

                ////log.Info($"ContentProvider BusyCount: {op.BusyCount}");
                while (op.BusyCount > 0)
                {
                    System.Threading.Thread.Sleep(500);
                    ////log.Info($"ContentProvider BusyCount: {op.BusyCount}");
                }

                // migrate weenies to database
                var old = op.WeenieSearch(null, new SearchWeeniesCriteria());
                if (old != null && old.Count > 0)
                {
                    log.Info($"Starting Weenie Migration: {old.Count} entries");

                    IWeenieProvider np = ContentProviderHost.GetProvider<IWeenieProvider>(WeenieChange.TypeName);
                    foreach (var wi in old)
                    {
                        try
                        {
                            Weenie w = op.GetWeenie(null, wi.WeenieClassId);
                            try
                            {
                                Weenie nw = np.Get(wi.WeenieClassId);
                                if (nw?.LastModified != null)
                                {
                                    op.DeleteWeenie(null, w.WeenieClassId);
                                    continue;
                                }
                            }
                            catch
                            {
                            }

                            if (np.Save(w))
                                op.DeleteWeenie(null, w.WeenieClassId);
                        }
                        catch (Exception ex)
                        {
                            log.Error("Failed to migrate weenie", ex);
                        }
                    }

                    log.Info($"Completed Weenie Migration");
                }

                var oldsb = osp.GetAllWeenieChanges();
                if (oldsb != null && oldsb.Count > 0)
                {
                    log.Info($"Starting Weenie Sandbox Migration: {oldsb.Count} entries");

                    IWeenieSandboxProvider np = SandboxContentProviderHost.GetProvider<IWeenieSandboxProvider>(WeenieChange.TypeName);
                    foreach (var wc in oldsb)
                    {
                        try
                        {
                            Guid uid = new Guid(wc.UserGuid);
                            np.UpdateChange(uid, wc);
                            osp.DeleteWeenieChange(wc.UserGuid, wc);
                        }
                        catch (Exception ex)
                        {
                            log.Error("Failed to migrate sandbox weenie", ex);
                        }
                    }

                    log.Info($"Completed Weenie Sandbox Migration");
                }

                var oldlb = op.GetLandblocks().ToList();
                if (oldlb != null && oldlb.Count() > 0)
                {
                    log.Info($"Starting SpawnMap Migration: {oldlb.Count()} entries");

                    ISpawnMapProvider np = ContentProviderHost.GetProvider<ISpawnMapProvider>(SpawnMapChange.TypeName);
                    foreach (var lb in oldlb)
                    {
                        try
                        {
                            if (np.Save(lb))
                                op.DeleteLandblock(lb.Key);
                        }
                        catch (Exception ex)
                        {
                            log.Error("Failed to migrate spawn map", ex);
                        }
                    }

                    log.Info($"Completed SpawnMap Migration");
                }

                var oldlbsb = osp.GetChanges(SpawnMapChange.TypeName).Cast<SpawnMapChange>().ToList();
                if (oldlbsb != null && oldlbsb.Count() > 0)
                {
                    log.Info($"Starting SpawnMap Sandbox Migration: {oldlbsb.Count()} entries");

                    ISpawnMapSandboxProvider np = SandboxContentProviderHost.GetProvider<ISpawnMapSandboxProvider>(SpawnMapChange.TypeName);
                    foreach (var lb in oldlbsb)
                    {
                        try
                        {
                            Guid uid = new Guid(lb.UserGuid);
                            np.UpdateChange(uid, lb);
                            osp.DeleteChange(uid, lb);
                        }
                        catch (Exception ex)
                        {
                            log.Error("Failed to migrate sandbox spawn map", ex);
                        }
                    }

                    log.Info($"Completed SpawnMap Sandbox Migration");
                }
            });
        }

        protected void Application_AuthorizeRequest(object sender, EventArgs e)
        {
        }

        protected void Application_EndRequest(object sender, System.EventArgs e)
        {
            // If the user is not authorised to see this page or access this function, send them to the error page.
            if (Response.StatusCode == (int)HttpStatusCode.Unauthorized && !Response.SuppressFormsAuthenticationRedirect)
            {
                Response.ClearContent();
                Response.RedirectToRoute("AuthHandler", (RouteTable.Routes["AuthHandler"] as Route).Defaults);
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception error = Server.GetLastError();
            CryptographicException cryptoEx = error as CryptographicException;

            // so, explanation is due here.  these Crypto exceptions fire when something about your cookie
            // goes bad.  changing ADFS password, machine key changes, cookie expires, etc.  we will
            // attempt to force a sign-out and if one of those are successful, we can clear the error.

            bool clear = false;

            if (cryptoEx != null)
            {
                try
                {
                    FederatedAuthentication.SessionAuthenticationModule.SignOut();
                    clear = true;
                }
                catch
                {
                    // swallow
                }

                try
                {
                    FormsAuthentication.SignOut();
                    clear = true;
                }
                catch
                {
                    // swallow
                }
            }

            // clear and move on.
            if (clear)
                Server.ClearError();
            else
            {
                // TODO: handle saving of exception
                // Server.Transfer("~/Error/Index");
            }
        }
    }
}

/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;

using Lifestoned.DataModel.Gdle;
using Lifestoned.DataModel.Gdle.Recipes;
using Lifestoned.DataModel.Gdle.Spawns;
using Lifestoned.DataModel.Gdle.Spells;
using Lifestoned.DataModel.Shared;
using Lifestoned.DataModel.WorldRelease;
using Lifestoned.Providers;

using DerethForever.Web.Models.Discord;

// TODO: Create a provider for this?

namespace DerethForever.Web.Controllers
{
    using IndexModel = Lifestoned.DataModel.WorldRelease.IndexModel;

    public class WorldReleaseController : BaseController
    {
        private static string TagName = ConfigurationManager.AppSettings["VersionTag"];
        private static string TagDateFormat = ConfigurationManager.AppSettings["VersionDateTimeFormat"];

        public static string GetDownloadFileName(ReleaseType type)
        {
            return $"{TagName}-{type}-{DateTime.Now.ToString(TagDateFormat)}.zip";
        }

        private string GetReleaseDir()
        {
            return Path.GetFullPath(ConfigurationManager.AppSettings["WorldReleaseDir"]);
        }

        // GET: Release
        public ActionResult Index(IndexModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                model.Results = new List<Release>();
                string releaseDir = GetReleaseDir();
                var allReleases = Directory.GetFiles(releaseDir, "*.json", SearchOption.AllDirectories).OrderByDescending(f => new FileInfo(f).CreationTime).ToList();

                foreach (var file in allReleases)
                {
                    Release r = JsonConvert.DeserializeObject<Release>(System.IO.File.ReadAllText(file));

                    if (!System.IO.File.Exists(Path.Combine(releaseDir, r.FileName)))
                    {
                        System.IO.File.Delete(file);
                        continue;
                    }

                    if (r.Type == ReleaseType.Test && !User.IsInRole("Admin"))
                        continue;

                    model.Results.Add(r);
                }
            }
            catch (Exception ex)
            {
                model.Results = null;
                model.ShowResults = false;
                model.ErrorMessages.Add("Error retrieving data from disk.");
                model.Exception = ex;
            }

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult Delete(string fileName)
        {
            if (fileName?.Length > 0)
            {
                try
                {
                    fileName = Path.GetFileName(fileName);

                    string releaseDir = GetReleaseDir();
                    string filePath = Path.GetFullPath(Path.Combine(releaseDir, fileName));

                    System.IO.File.Delete(filePath);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Get(string fileName)
        {
            if (fileName?.Length > 0)
            {
                try
                {
                    fileName = Path.GetFileName(fileName);

                    string releaseDir = GetReleaseDir();
                    string filePath = Path.GetFullPath(Path.Combine(releaseDir, fileName));
                    return File(filePath, "application/zip", fileName);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return HttpNotFound();
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult Cut()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Cut(string desc, ReleaseType type = ReleaseType.Test)
        {
            string token = GetUserToken();
            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            ZipFile archive = new ZipFile();

            // create a new zip package
            // go through the "providers" creating a folder for each in the zip
            // dump the updates from each provider into its folder

            ////List<WeenieSearchResult> weenieUpdates = ContentProviderHost.CurrentProvider.AllUpdates(token);
            ////foreach (WeenieSearchResult result in weenieUpdates)
            ////{
            ////    Weenie weenie = ContentProviderHost.CurrentProvider.GetWeenie(token, result.WeenieClassId);
            ////    string content = JsonConvert.SerializeObject(weenie, settings);

            ////    archive.AddFile($"weenies/{FormatFileName(weenie.WeenieClassId, weenie.Name)}", content);
            ////}
            var weenies = ContentProviderHost.GetContent(WeenieChange.TypeName).Cast<Weenie>().Where(w => w.LastModified.HasValue);
            foreach (Weenie weenie in weenies)
            {
                string content = JsonConvert.SerializeObject(weenie, settings);
                archive.AddFile($"weenies/{FormatFileName(weenie.WeenieClassId, weenie.Name)}", content);
            }

            ////var maps = ContentProviderHost.CurrentProvider.GetLandblocks().Where(m => m.LastModified.HasValue);
            ////foreach (SpawnMapEntry map in maps)
            ////{
            ////    string content = JsonConvert.SerializeObject(map, settings);
            ////    archive.AddFile($"spawnMaps/{FormatFileName(map.Key, map.Description)}", content);
            ////}
            var maps = ContentProviderHost.GetContent(SpawnMapChange.TypeName).Cast<SpawnMapEntry>().Where(s => s.LastModified.HasValue);
            foreach (SpawnMapEntry map in maps)
            {
                string content = JsonConvert.SerializeObject(map, settings);
                archive.AddFile($"spawnMaps/{FormatFileName(map.Key, map.Description)}", content);
            }

            ////var recipes = ContentProviderHost.CurrentRecipeProvider.Get().Where(r => r.LastModified.HasValue);
            var recipes = ContentProviderHost.GetContent(RecipeChange.TypeName).Cast<Recipe>().Where(r => r.LastModified.HasValue);
            foreach (Recipe recipe in recipes)
            {
                string content = JsonConvert.SerializeObject(recipe, settings);
                archive.AddFile($"recipes/{FormatFileName(recipe.Key, recipe.Description)}", content);
            }

            // TODO: Spell Table!?
            SpellsFile spellTable = new SpellsFile();
            var spells = ContentProviderHost.GetContent(SpellTableChange.TypeName).Cast<SpellTableEntry>().Where(s => s.LastModified.HasValue);
            foreach (SpellTableEntry spell in spells)
            {
                spellTable.Table.Entries.Add(spell);
            }

            {
                string content = JsonConvert.SerializeObject(spellTable, settings);
                archive.AddFile("spells.json", content);
            }

            string releaseDir = GetReleaseDir();
            string fileName = GetDownloadFileName(type);
            string filePath = Path.GetFullPath(Path.Combine(releaseDir, fileName));

            using (Stream package = archive.BuildZipStream())
            using (FileStream fs = System.IO.File.OpenWrite(Path.Combine(filePath)))
            {
                package.Seek(0, SeekOrigin.Begin);
                package.CopyTo(fs);
            }

            Release release = new Release();
            release.Description = desc;
            release.FileName = fileName;
            release.Type = type;
            release.ReleaseDateTime = DateTime.Now.ToString("yyyy-MM-dd");

            filePath = Path.GetFullPath(Path.Combine(releaseDir, $"{DateTime.Now.ToString("yyyy-MM-dd_hhmmss")}.json"));

            System.IO.File.WriteAllText(filePath, JsonConvert.SerializeObject(release, settings));

            if (type != ReleaseType.Test)
            {
                WorldReleaseEvent wre = new WorldReleaseEvent();
                wre.User = CurrentUser.DisplayName;
                wre.ReleaseTime = DateTimeOffset.Now;
                wre.Name = release.FileName;
                wre.Size = release.FileSize;
                DiscordController.PostToDiscordAsync(wre);
            }

            return RedirectToAction("Index", "Server");
        }
    }
}

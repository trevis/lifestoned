﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

using log4net;

namespace DerethForever.Web.Controllers
{
    public class EnumController : Controller
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static Assembly DataModelAssembly { get; set; }

        static EnumController()
        {
            DataModelAssembly = Assembly.GetAssembly(typeof(Lifestoned.DataModel.Shared.StringPropertyId));
        }

        [HttpGet]
        public JsonResult GetValues(string name)
        {
            try
            {
                Type et = DataModelAssembly.GetType("Lifestoned.DataModel.Shared." + name);

                var vals = et.GetFields(BindingFlags.Static | BindingFlags.Public)
                    .Where(f => f.GetCustomAttribute<DisplayAttribute>()?.GetAutoGenerateField() != false)
                    .Select(f => new KeyValuePair<Enum, string>(
                        (Enum)Enum.Parse(et, f.Name),
                        f.GetCustomAttribute<DisplayAttribute>()?.Name ?? f.Name));

                return Json(vals, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.Error("GetValues", ex);
                throw;
            }
        }
    }
}
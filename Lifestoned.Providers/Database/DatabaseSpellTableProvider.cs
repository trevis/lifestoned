﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using log4net;
using Newtonsoft.Json;

using Lifestoned.DataModel.Gdle.Spells;

namespace Lifestoned.Providers.Database
{
    public class DatabaseSpellTableProvider : SQLiteContentDatabase<SpellTableEntry>, ISpellTableProvider
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DatabaseSpellTableProvider() : base("SpellTableDbConnection", "Spells", (o) => o.Key)
        {
        }
    }

    public class DatabaseSpellTableSandboxProvider : SQLiteSandboxDatabase<SpellTableEntry, SpellTableChange>, ISpellTableSandboxProvider
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DatabaseSpellTableSandboxProvider() : base("SpellTableDbConnection", "Spells_Sandbox", (o) => o.Key)
        {
        }
    }
}

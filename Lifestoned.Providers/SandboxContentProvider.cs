/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using log4net;
using Newtonsoft.Json;
using Lifestoned.DataModel.Gdle;
using Lifestoned.DataModel.Shared;
using Lifestoned.DataModel.WorldRelease;
using Lifestoned.DataModel.Gdle.Recipes;
using Lifestoned.DataModel.Gdle.Spawns;
using Lifestoned.DataModel.Gdle.Spells;

namespace Lifestoned.Providers
{
    using CacheDictionaryUint = ConcurrentDictionary<Guid, Dictionary<uint, string>>;
    using CacheDictionaryGuid = ConcurrentDictionary<Guid, Dictionary<Guid, string>>;
    using DictionaryUint = Dictionary<uint, string>;
    using DictionaryGuid = Dictionary<Guid, string>;

    public class SandboxContentProvider : ISandboxContentProvider
    {
        private static readonly ILog log = LogManager.GetLogger("SandboxingContentProvider");

        private IContentProvider _backingProvider;

        private string _cacheDirectory;

        private CacheDictionaryUint _weenieCache;
        private CacheDictionaryGuid _recipeCache;
        private CacheDictionaryGuid _contentCache;
        private CacheDictionaryUint _spawnMapCache;

        public int BusyCount => 0;

        public SandboxContentProvider(IContentProvider backingProvider, string cacheDirectory)
        {
            _backingProvider = backingProvider;
            _cacheDirectory = cacheDirectory;

            ReloadCache();
        }

        public void ReloadCache()
        {
            if (!Directory.Exists(_cacheDirectory))
            {
                log.Error("caching directory does not exist: " + _cacheDirectory);
                log.Warn("caching is disabled.");
                return;
            }

            ReloadCacheData(ref _weenieCache, "weenies", uint.Parse);
            ReloadCacheData(ref _spawnMapCache, "spawnmaps", uint.Parse);

            // ReloadRecipeCache();
            // ReloadContentCache();
        }

        private void ReloadCacheData<TCacheEntry, TKey>(ref ConcurrentDictionary<Guid, TCacheEntry> cache, string folder, Func<string, TKey> parse)
            where TCacheEntry : IDictionary<TKey, string>, new()
            where TKey : IComparable
        {
            cache = new ConcurrentDictionary<Guid, TCacheEntry>();
            string rootPath = Path.Combine(_cacheDirectory, folder);

            if (!Directory.Exists(rootPath))
                Directory.CreateDirectory(rootPath);

            // build cache from disk
            var userDirectories = Directory.EnumerateDirectories(rootPath);

            foreach (var userDir in userDirectories)
            {
                Guid userGuid;
                string userGuidString = Path.GetFileName(userDir);
                if (!Guid.TryParse(userGuidString, out userGuid))
                    continue;

                TCacheEntry userCache = default(TCacheEntry);
                if (!cache.TryGetValue(userGuid, out userCache))
                {
                    userCache = new TCacheEntry();
                    cache.TryAdd(userGuid, userCache);
                }

                var files = Directory.EnumerateFiles(userDir, "*.json");
                foreach (string file in files)
                {
                    try
                    {
                        string filename = Path.GetFileNameWithoutExtension(file);
                        // file is "{WeenieID}.json"
                        TKey id = parse(filename);

                        if (id.CompareTo(default(TKey)) == 0)
                            continue;

                        userCache.Add(id, file);
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Error loading {file} from {folder} cache.", ex);
                    }
                }
            }
        }

        private void ReloadRecipeCache()
        {
            _recipeCache = new ConcurrentDictionary<Guid, Dictionary<Guid, string>>();
            string cacheFolder = Path.Combine(_cacheDirectory, "recipes");

            if (!Directory.Exists(cacheFolder))
                Directory.CreateDirectory(cacheFolder);

            // build cache from disk
            var userDirectories = Directory.EnumerateDirectories(cacheFolder);

            foreach (var userDir in userDirectories)
            {
                Guid userGuid;

                if (!Guid.TryParse(userDir, out userGuid))
                    continue;

                _recipeCache.TryAdd(userGuid, new Dictionary<Guid, string>());
                string userPath = Path.Combine(cacheFolder, userDir);

                var files = Directory.EnumerateFiles(userPath, "*.json");
                foreach (string file in files)
                {
                    try
                    {
                        // file is "{RecipeGuid}.json"
                        int begin = file.LastIndexOf("\\") + 1;
                        int end = file.IndexOf(".", begin);

                        string recipeGuidString = file.Substring(begin, end - begin);
                        Guid recipeGuid = Guid.Parse(recipeGuidString);

                        _recipeCache[userGuid].Add(recipeGuid, Path.Combine(userPath, file));
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Error loading {file} from recipe cache.", ex);
                    }
                }
            }
        }

        private void ReloadContentCache()
        {
            _contentCache = new ConcurrentDictionary<Guid, Dictionary<Guid, string>>();
            string cacheFolder = Path.Combine(_cacheDirectory, "content");

            if (!Directory.Exists(cacheFolder))
                Directory.CreateDirectory(cacheFolder);

            // build cache from disk
            var userDirectories = Directory.EnumerateDirectories(cacheFolder);

            foreach (var userDir in userDirectories)
            {
                Guid userGuid;

                if (!Guid.TryParse(userDir, out userGuid))
                    continue;

                _contentCache.TryAdd(userGuid, new Dictionary<Guid, string>());
                string userPath = Path.Combine(cacheFolder, userDir);

                var files = Directory.EnumerateFiles(userPath, "*.json");
                foreach (string file in files)
                {
                    try
                    {
                        // file is "{ContentGuid}.json"
                        int begin = file.LastIndexOf("\\") + 1;
                        int end = file.IndexOf(".", begin);

                        string contentGuidString = file.Substring(begin, end - begin);
                        Guid contentGuid = Guid.Parse(contentGuidString);

                        _contentCache[userGuid].Add(contentGuid, Path.Combine(userPath, file));
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Error loading {file} from content cache.", ex);
                    }
                }
            }
        }

        private string GetUserPath(Guid userId, string folder)
        {
            string path = Path.Combine(_cacheDirectory, folder, userId.ToString());

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        private string GetUserPath(Guid userId, string folder, uint id)
        {
            return Path.Combine(GetUserPath(userId, folder), $"{id}.json");
        }

        private string GetUserPath(string token, string folder)
        {
            string userGuidString = JwtCookieManager.GetUserGuid(token);
            string path = Path.Combine(_cacheDirectory, folder, userGuidString);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        private string GetUserPath(string token, string folder, uint id)
        {
            return Path.Combine(GetUserPath(token, folder), $"{id}.json");
        }

        private IEnumerable<ChangeEntry> YieldEntries<T, U>(CacheDictionaryUint cache)
            where T : ChangeEntry<U>, new()
            where U : class, IMetadata
        {
            foreach (DictionaryUint userCache in cache.Values)
            {
                foreach (string path in userCache.Values)
                {
                    T w = JsonConvert.DeserializeObject<T>(File.ReadAllText(path));
                    if (w != null && w.Entry != null)
                        yield return w as ChangeEntry;
                }
            }
        }

        private IEnumerable<ChangeEntry> YieldEntries<T, U>(CacheDictionaryUint cache, Guid userId)
            where T : ChangeEntry<U>, new()
            where U : class, IMetadata
        {
            if (cache.TryGetValue(userId, out DictionaryUint userCache))
            {
                foreach (string path in userCache.Values)
                {
                    T w = JsonConvert.DeserializeObject<T>(File.ReadAllText(path));
                    if (w != null && w.Entry != null)
                        yield return w as ChangeEntry;
                }
            }
        }

        private IEnumerable<IEnumerable<ChangeEntry>> GetChangeSources()
        {
            yield return YieldEntries<WeenieChange, Weenie>(_weenieCache);
            yield return YieldEntries<SpawnMapChange, SpawnMapEntry>(_spawnMapCache);
            ////yield return SandboxContentProviderHost.CurrentSpellTableProvider.GetChanges();
            ////yield return SandboxContentProviderHost.CurrentRecipeProvider.GetChanges();
        }

        private IEnumerable<IEnumerable<ChangeEntry>> GetChangeSources(Guid userId)
        {
            yield return YieldEntries<WeenieChange, Weenie>(_weenieCache, userId);
            yield return YieldEntries<SpawnMapChange, SpawnMapEntry>(_spawnMapCache, userId);
            ////yield return SandboxContentProviderHost.CurrentSpellTableProvider.GetChanges(userId);
            ////yield return SandboxContentProviderHost.CurrentRecipeProvider.GetChanges(userId);
        }

        public IEnumerable<ChangeEntry> GetChanges()
        {
            return GetChangeSources().SelectMany(o => o);
            //return Enumerable.Concat(Enumerable.Concat(
            //    YieldEntries<WeenieChange, Weenie>(_weenieCache),
            //    YieldEntries<SpawnMapChange, SpawnMapEntry>(_spawnMapCache)),
            //    SandboxContentProviderHost.CurrentSpellTableProvider.GetChanges());
        }

        public IEnumerable<ChangeEntry> GetChanges(string type)
        {
            // TODO: Abstract away type (attributes?)
            switch (type)
            {
                case WeenieChange.TypeName:
                    return YieldEntries<WeenieChange, Weenie>(_weenieCache);

                case SpawnMapChange.TypeName:
                    return YieldEntries<SpawnMapChange, SpawnMapEntry>(_spawnMapCache);

                default:
                    return Enumerable.Empty<ChangeEntry>();
            }
        }

        public IEnumerable<ChangeEntry> GetChanges(Guid userId)
        {
            return GetChangeSources(userId).SelectMany(o => o);
            //return
            //    Enumerable.Concat(
            //        Enumerable.Concat(
            //            Enumerable.Concat(
            //                YieldEntries<WeenieChange, Weenie>(_weenieCache, userId),
            //                YieldEntries<SpawnMapChange, SpawnMapEntry>(_spawnMapCache, userId)),
            //            SandboxContentProviderHost.CurrentSpellTableProvider.GetChanges(userId)),
            //        SandboxContentProviderHost.CurrentRecipeProvider.GetChanges(userId)
            //        );
        }

        public IEnumerable<ChangeEntry> GetChanges(Guid userId, string type)
        {
            // TODO: Abstract away type (attributes?)
            switch (type)
            {
                case WeenieChange.TypeName:
                    return YieldEntries<WeenieChange, Weenie>(_weenieCache, userId);

                case SpawnMapChange.TypeName:
                    return YieldEntries<SpawnMapChange, SpawnMapEntry>(_spawnMapCache, userId);

                ////case SpellTableChange.TypeName:
                ////    return SandboxContentProviderHost.CurrentSpellTableProvider.GetChanges(userId);

                ////case RecipeChange.TypeName:
                ////    return SandboxContentProviderHost.CurrentRecipeProvider.GetChanges(userId);

                default:
                    return Enumerable.Empty<ChangeEntry>();
            }
        }

        public void UpdateChange(Guid userId, ChangeEntry change)
        {
            string path = null;

            // TODO: Abstract away type (attributes?)
            switch (change.EntryType)
            {
                case WeenieChange.TypeName:
                    path = "weenies";
                    break;

                case SpawnMapChange.TypeName:
                    path = "spawnmaps";
                    break;

                default:
                    return;
            }

            path = GetUserPath(userId, path, change.EntryId);
            string content = JsonConvert.SerializeObject(change);
            File.WriteAllText(path, content);
        }

        public void AcceptChange(Guid userId, ChangeEntry change)
        {
            // TODO: Abstract away type (attributes?)
            switch (change.EntryType)
            {
                case WeenieChange.TypeName:
                    WeenieChange wc = change as WeenieChange;
                    _backingProvider.UpdateWeenie(null, wc.Entry);
                    break;

                case SpawnMapChange.TypeName:
                    SpawnMapChange sc = change as SpawnMapChange;
                    _backingProvider.SaveLandblock(sc.Entry);
                    break;

                default:
                    return;
            }
        }

        public void DeleteChange(Guid userId, ChangeEntry change)
        {
            string path = null;
            DictionaryUint cache = null;

            // TODO: Abstract away type (attributes?)
            switch (change.EntryType)
            {
                case WeenieChange.TypeName:
                    _weenieCache.TryGetValue(userId, out cache);
                    path = "weenies";
                    break;

                case SpawnMapChange.TypeName:
                    _spawnMapCache.TryGetValue(userId, out cache);
                    path = "spawnmaps";
                    break;

                default:
                    return;
            }

            path = GetUserPath(userId, path, change.EntryId);

            if (File.Exists(path))
                File.Delete(path);

            cache.Remove(change.EntryId);
        }

        public List<WeenieChange> GetAllWeenieChanges()
        {
            List<WeenieChange> everything = new List<WeenieChange>();

            foreach (var c in _weenieCache)
            {
                foreach (var file in c.Value.Values)
                {
                    var wc = JsonConvert.DeserializeObject<WeenieChange>(File.ReadAllText(file));
                    if (wc != null)
                        everything.Add(wc);
                }
            }

            return everything;
        }

        public List<WeenieChange> GetMyWeenieChanges(string token)
        {
            string userGuidString = JwtCookieManager.GetUserGuid(token);
            Guid userGuid = Guid.Parse(userGuidString);
            List<WeenieChange> mine = new List<WeenieChange>();

            if (_weenieCache.ContainsKey(userGuid))
            {
                var c = _weenieCache[userGuid];
                foreach (var file in c.Values)
                {
                    var wc = JsonConvert.DeserializeObject<WeenieChange>(File.ReadAllText(file));
                    if (wc != null)
                        mine.Add(wc);
                }
            }

            return mine;
        }

        public byte[] GetWorldRelease(string token, string fileName)
        {
            return _backingProvider.GetWorldRelease(token, fileName);
        }

        public byte[] GetCurrentWorldRelease(string token)
        {
            return _backingProvider.GetCurrentWorldRelease(token);
        }

        public Release GetCurrentWorldReleaseInfo(string token)
        {
            return _backingProvider.GetCurrentWorldReleaseInfo(token);
        }

        public Release GetWorldReleaseInfo(string token, string fileName)
        {
            return _backingProvider.GetWorldReleaseInfo(token, fileName);
        }

        public Weenie GetWeenie(string token, uint weenieClassId)
        {
            if (token != null)
            {
                string userGuidString = JwtCookieManager.GetUserGuid(token);

                Guid userGuid = Guid.Parse(userGuidString);

                if (_weenieCache != null && _weenieCache.ContainsKey(userGuid) &&
                    _weenieCache[userGuid].ContainsKey(weenieClassId))
                {
                    string file = _weenieCache[userGuid][weenieClassId];
                    WeenieChange wc = JsonConvert.DeserializeObject<WeenieChange>(File.ReadAllText(file));
                    return wc.Entry;
                }
            }

            return _backingProvider.GetWeenie(token, weenieClassId);
        }

        public Weenie GetWeenieFromSource(string token, uint weenieClassId)
        {
            return _backingProvider.GetWeenie(token, weenieClassId);
        }

        public bool UpdateWeenie(string token, Weenie weenie)
        {
            return SaveWeenie(token, weenie);
        }

        public List<WeenieSearchResult> WeenieSearch(string token, SearchWeeniesCriteria criteria)
        {
            return _backingProvider.WeenieSearch(token, criteria);
        }

        public List<WeenieSearchResult> RecentChanges(string token)
        {
            return _backingProvider.RecentChanges(token);
        }

        public List<WeenieSearchResult> AllUpdates(string token)
        {
            return _backingProvider.AllUpdates(token);
        }

        public bool CreateWeenie(string token, Weenie weenie)
        {
            return SaveWeenie(token, weenie);
        }

        private bool SaveWeenie(string token, Weenie weenie)
        {
            if (_weenieCache == null)
                throw new ApplicationException("Sandboxing is not configured correctly.  See error logs for details.");

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;

            string userGuidString = JwtCookieManager.GetUserGuid(token);
            Guid userGuid = Guid.Parse(userGuidString);
            string weenieFolder = Path.Combine(_cacheDirectory, "weenies", userGuid.ToString());

            if (!Directory.Exists(weenieFolder))
                Directory.CreateDirectory(weenieFolder);

            WeenieChange wc = null;
            string weenieFile = Path.Combine(weenieFolder, $"{weenie.WeenieClassId}.json");
            if (File.Exists(weenieFile))
            {
                // replace the weenie
                wc = JsonConvert.DeserializeObject<WeenieChange>(File.ReadAllText(weenieFile));
                wc.Entry = weenie;
                wc.Submitted = false;
            }
            else
            {
                wc = new WeenieChange();
                wc.UserGuid = JwtCookieManager.GetUserGuid(token);
                wc.Entry = weenie;
                wc.UserName = JwtCookieManager.GetUserDisplayName(token);
                wc.SubmissionTime = DateTime.Now;
            }

            string content = JsonConvert.SerializeObject(wc, settings);
            File.WriteAllText(weenieFile, content);

            if (!_weenieCache.ContainsKey(userGuid))
                _weenieCache.TryAdd(userGuid, new Dictionary<uint, string>());

            if (!_weenieCache[userGuid].ContainsKey(weenie.WeenieClassId))
                _weenieCache[userGuid].Add(weenie.WeenieClassId, weenieFile);

            return true;
        }

        public bool DeleteWeenie(string token, uint weenieClassId)
        {
            return _backingProvider.DeleteWeenie(token, weenieClassId);
        }

        public WeenieChange GetSandboxedChange(Guid userGuid, uint weenieClassId)
        {
            if (_weenieCache.ContainsKey(userGuid) && _weenieCache[userGuid].ContainsKey(weenieClassId))
            {
                string fileName = _weenieCache[userGuid][weenieClassId];
                WeenieChange wc = JsonConvert.DeserializeObject<WeenieChange>(File.ReadAllText(fileName));
                return wc;
            }

            return null;
        }

        public WeenieChange GetMySandboxedChange(string token, uint weenieClassId)
        {
            string userGuidString = JwtCookieManager.GetUserGuid(token);
            Guid userGuid = Guid.Parse(userGuidString);
            if (_weenieCache.ContainsKey(userGuid) && _weenieCache[userGuid].ContainsKey(weenieClassId))
            {
                string fileName = _weenieCache[userGuid][weenieClassId];
                WeenieChange wc = JsonConvert.DeserializeObject<WeenieChange>(File.ReadAllText(fileName));
                return wc;
            }

            return null;
        }

        public void UpdateWeenieChange(string changeOwnerGuid, WeenieChange wc)
        {
            string userGuidString = changeOwnerGuid;
            uint wcid = wc.Entry.WeenieClassId;
            Guid userGuid = Guid.Parse(userGuidString);

            string weenieFolder = Path.Combine(_cacheDirectory, "weenies", userGuid.ToString());

            if (!Directory.Exists(weenieFolder))
                Directory.CreateDirectory(weenieFolder);

            string weenieFile = Path.Combine(weenieFolder, $"{wcid}.json");
            string content = JsonConvert.SerializeObject(wc);
            File.WriteAllText(weenieFile, content);
        }

        public void DeleteWeenieChange(string changeOwnerGuid, WeenieChange wc)
        {
            string userGuidString = changeOwnerGuid;
            uint wcid = wc.Entry.WeenieClassId;
            Guid userGuid = Guid.Parse(userGuidString);

            string weenieFolder = Path.Combine(_cacheDirectory, "weenies", userGuid.ToString());

            if (!Directory.Exists(weenieFolder))
                Directory.CreateDirectory(weenieFolder);

            if (_weenieCache[userGuid].ContainsKey(wc.Entry.WeenieClassId))
                _weenieCache[userGuid].Remove(wc.Entry.WeenieClassId);

            string weenieFile = Path.Combine(weenieFolder, $"{wcid}.json");
            if (File.Exists(weenieFile))
                File.Delete(weenieFile);
        }

        public bool UpdateWeenieInSource(string token, Weenie weenie)
        {
            return _backingProvider.UpdateWeenie(token, weenie);
        }

        public Release CutWorldRelease(string token, ReleaseType releaseType)
        {
            return _backingProvider.CutWorldRelease(token, releaseType);
        }

        #region SpawnMaps

        public SpawnMapChange GetLandblockChange(uint id)
        {
            string token = JwtCookieManager.GetToken();
            if (!string.IsNullOrEmpty(token))
            {
                string userGuidString = JwtCookieManager.GetUserGuid(token);
                return GetLandblockChange(id, userGuidString);
            }
            return null;
        }

        public SpawnMapChange GetLandblockChange(uint id, string userId)
        {
            Guid userGuid = Guid.Parse(userId);

            DictionaryUint cache = null;
            if (_spawnMapCache?.TryGetValue(userGuid, out cache) == true)
            {
                if (cache.TryGetValue(id, out string file))
                {
                    SpawnMapChange change = JsonConvert.DeserializeObject<SpawnMapChange>(File.ReadAllText(file));
                    return change;
                }
            }
            return null;
        }

        public SpawnMapEntry GetLandblock(uint id)
        {
            //SpawnMapChange change = GetLandblockChange(id);
            //if (change != null)
            //    return change.Entry;
            return _backingProvider.GetLandblock(id);
        }

        public void SaveLandblock(SpawnMapEntry spawnMap)
        {
            if (_spawnMapCache == null)
                throw new ApplicationException("Sandboxing is not configured correctly.  See error logs for details.");

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;

            string token = JwtCookieManager.GetToken();
            Guid userGuid = Guid.Parse(JwtCookieManager.GetUserGuid(token));

            string file = GetUserPath(userGuid, "spawnmaps", spawnMap.Key);

            SpawnMapChange wc = null;
            if (File.Exists(file))
            {
                // replace the weenie
                wc = JsonConvert.DeserializeObject<SpawnMapChange>(File.ReadAllText(file));
                wc.Entry = spawnMap;
                wc.Submitted = false;
            }
            else
            {
                wc = new SpawnMapChange();
                wc.UserGuid = JwtCookieManager.GetUserGuid(token);
                wc.Entry = spawnMap;
                wc.UserName = JwtCookieManager.GetUserDisplayName(token);
                wc.SubmissionTime = DateTime.Now;
            }

            string content = JsonConvert.SerializeObject(wc, settings);
            File.WriteAllText(file, content);

            DictionaryUint userCache = null;
            if (!_spawnMapCache.TryGetValue(userGuid, out userCache))
            {
                userCache = new DictionaryUint();
                _spawnMapCache.TryAdd(userGuid, userCache);
            }

            if (!userCache.ContainsKey(spawnMap.Key))
                userCache.Add(spawnMap.Key, file);
        }

        public void DeleteLandblock(uint id)
        {
            if (_spawnMapCache == null)
                throw new ApplicationException("Sandboxing is not configured correctly.  See error logs for details.");

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;

            string token = JwtCookieManager.GetToken();
            Guid userGuid = Guid.Parse(JwtCookieManager.GetUserGuid(token));

            string file = GetUserPath(userGuid, "spawnmaps", id);

            if (File.Exists(file))
            {
                File.Delete(file);

                DictionaryUint userCache = null;
                if (_spawnMapCache.TryGetValue(userGuid, out userCache))
                {
                    userCache.Remove(id);
                }
            }
        }

        public List<SpawnMapEntry> SearchLandblocks(string query)
        {
            return _backingProvider.SearchLandblocks(query);
        }

        public IEnumerable<SpawnMapEntry> GetLandblocks()
        {
            return _backingProvider.GetLandblocks();
        }

        #endregion
    }
}
